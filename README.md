# React native mahdi picker

A simple Persian Date Picker

## android Installation

# yarn

`yarn add mahdi-picker`

`yarn add mahdi-wheel`

# npm

`npm install mahdi-picker`

`npm install mahdi-wheel`

# ios

`npm install @react-native-community/picker`

pod install

# Usage

```js

import React, {useState} from "react";
import {
    Text,
    View,
    Pressable
} from "react-native";
import DatePicker from 'mahdi-picker'

const App = () => {

    const [visible, setVisible] = useState(false);

    return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>

            <DatePicker
                visible={visible}
                itemTextColor='rgb(100, 100, 100)'      // dont use hex color
                selectedItemTextColor='black'           // dont use hex color
                selectedItemBorderColor='black'
                textColor='rgb(0,0,0)'
                backgroundColor='#37f507'
                mainBackgroundColor='white'
                fontFamily='Roboto'
                fontSize={16}
                future={false}
                beforeMaxYear={50}
                futureMaxYear={10}
                borderRadius={10}
                onRequestClose={() => setVisible(false)}
                onPressAction={(date) => {
                    setVisible(false)
                }}
            />
            <Pressable onPress={() => setVisible(true)}>
                <Text style={{fontFamily: 'Roboto'}}>Choose date</Text>
            </Pressable>
        </View>

    );
};

export default App;
```

# Props

|**Prop**               |**Type**            |**Default Value**              |
|:---------------------:|:------------------:|:------------------------------:|
|visible                |boolean             |false                           |
|backgroundColor        |string              |green                           |
|mainBackgroundColor    |string              |white                           |
|textColor              |string              |white                           |
|fontFamily             |string              |undefined                       |
|fontSize               |number              |16                              |
|selectedItemTextColor  |string              |black (hex colors not supported)|
|itemTextColor          |string              |grey (hex colors not supported) |
|selectedItemBorderColor|string              |white                           |
|future                 |boolean             |false                           |
|futureMaxYear          |number              |20                              |
|beforeMaxYear          |number              |20                              |
|borderRadius           |number              |10                              |
|onRequestClose         |function            |null                            |
|onPressAction          |function            |null                            |